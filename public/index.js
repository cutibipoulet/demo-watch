
function mul(matrix1,matric2){
    return fabric.util.multiplyTransformMatrices(matrix1,matric2,false);
}

EasingFunctions = {
    // no easing, no acceleration
    linear: function (t) { return t },
    // accelerating from zero velocity
    easeInQuad: function (t) { return t*t },
    // decelerating to zero velocity
    easeOutQuad: function (t) { return t*(2-t) },
    // acceleration until halfway, then deceleration
    easeInOutQuad: function (t) { return t<.5 ? 2*t*t : -1+(4-2*t)*t },
    // accelerating from zero velocity
    easeInCubic: function (t) { return t*t*t },
    // decelerating to zero velocity
    easeOutCubic: function (t) { return (--t)*t*t+1 },
    // acceleration until halfway, then deceleration
    easeInOutCubic: function (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 },
    // accelerating from zero velocity
    easeInQuart: function (t) { return t*t*t*t },
    // decelerating to zero velocity
    easeOutQuart: function (t) { return 1-(--t)*t*t*t },
    // acceleration until halfway, then deceleration
    easeInOutQuart: function (t) { return t<.5 ? 8*t*t*t*t : 1-8*(--t)*t*t*t },
    // accelerating from zero velocity
    easeInQuint: function (t) { return t*t*t*t*t },
    // decelerating to zero velocity
    easeOutQuint: function (t) { return 1+(--t)*t*t*t*t },
    // acceleration until halfway, then deceleration
    easeInOutQuint: function (t) { return t<.5 ? 16*t*t*t*t*t : 1+16*(--t)*t*t*t*t }
}

class Watch {



    constructor(canvasContainer) {

        //properties
        this.GLOBAL_ROT_OFFSET = 180;

        this.canvasWidth = 500;
        this.canvasHeight = 500;

        this.currentSecondes;
        this.currentMinutes;
        this.currentHours;

        this.canvas;

        this.hours;
        this.minutes;
        this.seconds;

        this.hours2;
        this.minutes2;
        this.seconds2;


        /*
            top = -60;
            left = 198;
            skewX = 182;
            skewY = 177;
            scale = 0.28;
            angle = 324;
        */
        this.top = 57;
        this.left = 172;
        this.skewX = 0;
        this.skewY = 0;
        this.scale = 0.32;
        this.angle = 320;

        this.transformMatrix = [ 1, 0, 0, 1, 0, 0 ];

        this.currentDateSeconds = new Date();
        this.currentDateMinutes = new Date();
        this.currentDateHours = new Date();
        this.dateToGo = new Date();


        this.offsetTimeSeconds = 50;//1000*3600*2;
        this.offsetTimeMinutes = 30;//1000*3600*2;
        this.offsetTimeHours = 50;//1000*3600*2;
        this.modeStatic = false;



        //controller code
        this.initWatch(canvasContainer);
        this.currentDateSeconds = new Date( (new Date)*1 - this.offsetTimeSeconds * 3000 );
        this.currentDateMinutes = new Date( (new Date)*1 - this.offsetTimeMinutes * 20 * 1000 );
        this.currentDateHours = new Date( (new Date)*1 - this.offsetTimeHours * 5 * 60 * 1000);
    }

    initWatch(canvasContainer) {
        const canvasEl = document.createElement("canvas");
        canvasEl.width = this.canvasWidth;
        canvasEl.height = this.canvasHeight;

        canvasContainer.appendChild(canvasEl);
        this.canvas = new fabric.StaticCanvas(canvasEl);

        this.canvas.add(this.getBg());

        this.seconds = this.getNeedle("seconds");
        this.minutes = this.getNeedle("minutes");
        this.hours = this.getNeedle("hours");

        this.seconds2 = this.getNeedle("seconds2");
        this.minutes2 = this.getNeedle("minutes2");
        this.hours2 = this.getNeedle("hours2");

     //   var gradient = this.canvas.createRadialGradient(105, 105, 20, 120, 120, 50);
     //   gradient.addColorStop(0, 'rgba(250,250,255,0)');
     //   gradient.addColorStop(0.75, 'rgba(230,250,255,1.0)');
     //   gradient.addColorStop(1, 'rgba(0,0,255,0)');
//

        var glass = new fabric.Circle({
            left: 299,
            top: 185,
            radius: 130,
            opacity: 0.4,
            originX:"center",
            originY:"center",

        });
        //105, 105, 20, 160, 120, 50
        glass.setGradient('fill', {
            x1: 105,
            y1: 20,
            x2: 250,
            y2: 230,
            colorStops: {
                0: 'rgba(250,250,255,0)',
                0.75: 'rgba(230,230,230,1.0)',
                1: 'rgba(200,200,255,0.2)'
            }
        });


        this.hours.setShadow({
            blur: 15,
            color: 'rgba(0,0,0,0.6)',
            offsetX: 2,
            offsetY: 2
        });

        this.minutes.setShadow({
            blur: 15,
            color: 'rgba(0,0,0,0.6)',
            offsetX: 4,
            offsetY: 4
        });

        this.seconds.setShadow({
            blur: 15,
            color: 'rgba(0,0,0,0.6)',
            offsetX: 7,
            offsetY: 7
        });

        this.hours2.setShadow({
            blur: 15,
            color: 'rgba(0,0,0,0.6)',
            offsetX: 2,
            offsetY: 2
        });

        this.minutes2.setShadow({
            blur: 15,
            color: 'rgba(0,0,0,0.6)',
            offsetX: 4,
            offsetY: 4
        });

        this.seconds2.setShadow({
            blur: 15,
            color: 'rgba(0,0,0,0.6)',
            offsetX: 7,
            offsetY: 7
        });


        this.canvas.add(this.hours);
        this.canvas.add(this.hours2);
        this.canvas.add(this.minutes);
        this.canvas.add(this.minutes2);
        this.canvas.add(this.seconds);
        this.canvas.add(this.seconds2);





        this.canvas.add(this.getBgHover());

      // this.canvas.add(glass);

        this.canvas.renderAll();

        this.tick();

    }

    tick(){
        const _this = this;
        this.calculateTime();
        this.buildCurrent();
        setTimeout(function (){_this.tick()},50);
    }


    diffInMs (d1,d2){
        return (d1.getTime() - d2.getTime());
    }
    diffInMinutes (d1,d2){
        return (d1.getTime() - d2.getTime()) / (1000 * 60);
    }
    diffInHours (d1,d2){
        return (d1.getTime() - d2.getTime()) / (1000 * 60 * 60);
    }




    showBrand(){
        if(!this.modeStatic){
            const d = new Date();
            d.setHours(22);
            d.setMinutes(10);
            d.setSeconds(13);
            this.dateToGo = d;
            this.modeStatic = true;
        }
    }
    showTime(){
        this.modeStatic = false;
    }

    stop(){
        this.modeStatic = true;
    }


    calculateTime(){

        if(!this.modeStatic){
            this.dateToGo = new Date();
        }


        let distanceInMs = this.diffInMs(this.dateToGo,this.currentDateSeconds);
        let nextMs = Math.max(-1,Math.min(1,EasingFunctions.easeOutQuint(distanceInMs / (this.offsetTimeSeconds*1000)))) * 6000;
        this.currentDateSeconds.setTime(this.currentDateSeconds.getTime() + nextMs);
        this.currentSecondes = this.currentDateSeconds.getSeconds() + this.currentDateSeconds.getMilliseconds()/1000;
        if(distanceInMs> 60 * 1000 || distanceInMs< -60 * 1000){
            this.currentDateSeconds.setTime(this.currentDateSeconds.getTime() +   Math.floor(distanceInMs/60000) * 60 * 1000)
        }


        let distanceInMsForMinutes = this.diffInMinutes(this.dateToGo,this.currentDateMinutes);
        let nextMsForMinutes = Math.max(-1,Math.min(1,EasingFunctions.easeOutQuint(distanceInMsForMinutes / this.offsetTimeMinutes))) * 4;
        this.currentDateMinutes.setTime(this.currentDateMinutes.getTime() + nextMsForMinutes * 60 * 1000);
        this.currentMinutes = this.currentDateMinutes.getMinutes() + this.currentDateMinutes.getSeconds()/60;
        if(distanceInMsForMinutes> 60 || distanceInMsForMinutes< -60){
            this.currentDateMinutes.setTime(this.currentDateMinutes.getTime() +   Math.floor(distanceInMsForMinutes/60) * 60 * 60 * 1000)
        }


        let distanceInMsForHours = this.diffInHours(this.dateToGo,this.currentDateHours);
        let nextMsForHours = Math.max(-1,Math.min(1,EasingFunctions.easeOutQuint(distanceInMsForHours / (this.offsetTimeHours)))) * 6;
        this.currentDateHours.setTime(this.currentDateHours.getTime() +nextMsForHours * 3600 * 1000);
        this.currentHours = this.currentDateHours.getHours() + this.currentDateHours.getMinutes()/60;


    }

    buildCurrent(){


        let elems = [this.seconds,this.minutes,this.hours,this.seconds2,this.minutes2,this.hours2];

        var secondRotation = this.currentSecondes * 360 / 60 + this.GLOBAL_ROT_OFFSET;
        var minutesRotation = this.currentMinutes * 360 / 60 + this.GLOBAL_ROT_OFFSET;
        var hourRotation = this.currentHours * 360 / 12 + this.GLOBAL_ROT_OFFSET;

 //       0 -> 15  -> 30 -> 45  -> 60
 // 2=>   1 -> 0.5 -> 0  -> 0.5 -> 1
 // 1=>   0 -> 0.5 -> 1  -> 0.5 -> 0
//
        var opacity2 = 0;
        if(this.currentSecondes < 30){
            opacity2 = 1 - this.currentSecondes / 30;
        }else if(this.currentSecondes >= 30){
            opacity2 =  1 - (30 - this.currentSecondes%30) / 30
        }
        this.seconds2.set('opacity', opacity2);
        this.seconds.set('opacity',1);

        for(let i=0;i<elems.length;i++) {
            var matrixCorrection = [1, 0, 0, 1, 0, 736 / 2 - 210];
            var translation2 = [1, 0, 0, 1, -118 / 2, -736 / 2 - 80];
            var translation2Bis = [1, 0, 0, 1, -0, 0];
            var translation = [1, 0, 0, 1, 250, 250];
            if(i==0 || i==3){
                var rotationMatrix = this.getRotationMatrix(secondRotation);//
            }else if(i==1|| i==4){
                var rotationMatrix = this.getRotationMatrix(minutesRotation);//
            }else if(i==2|| i==5){
                var rotationMatrix = this.getRotationMatrix(hourRotation);//
            }
            var completeMatrix = [1, 0, 0, 1, 0, 0];
            completeMatrix = mul(completeMatrix, translation);
            completeMatrix = mul(completeMatrix, translation2);

            //SkrewX an Y
            completeMatrix = mul(completeMatrix, [1,0,Math.tan(this.toRadian(this.skewX)),1,0,0]);
            completeMatrix = mul(completeMatrix, [1,Math.tan(this.toRadian(this.skewY)),0,1,0,0]);
            //Scale
            completeMatrix = mul(completeMatrix, [this.scale, 0, 0, this.scale, 0, 0]);
            //Translate
            completeMatrix = mul(completeMatrix, [1, 0, 0, 1, this.left, this.top]);
            //Rotate
            completeMatrix = mul(completeMatrix, this.getRotationMatrix(this.angle));


            completeMatrix = mul(completeMatrix, rotationMatrix);
            completeMatrix = mul(completeMatrix, matrixCorrection);
            completeMatrix = mul(completeMatrix, translation2Bis);

            elems[i].transformMatrix = completeMatrix;
        }


        this.canvas.renderAll();
    }


    toRadian(angleDegree){
        return angleDegree * Math.PI / 180;
    }

    getRotationMatrix(angleDegree){
        const angle = this.toRadian(angleDegree);
        return [Math.cos(angle),Math.sin(angle),-Math.sin(angle),Math.cos(angle),0,0];
    }

    getNeedle(kind){
        var imgElement = document.createElement("img");
        imgElement.src="assets/"+kind+".png";
        var imgInstance = new fabric.Image(imgElement, {
            centeredRotation : false,
            centerTransform: false,
            originX:"left",
            originY:"top",
           // angle: 0,
            opacity: 1
        });

        return imgInstance;
    }

    getBg(){
        var imgElement = document.createElement("img");
        imgElement.src="assets/tc2.png";
        var imgInstance = new fabric.Image(imgElement, {
        });
        return imgInstance;
    }
    getBgHover(){
        var imgElement = document.createElement("img");
        imgElement.src="assets/tc2-hover.png";
        var imgInstance = new fabric.Image(imgElement, {
        });
        return imgInstance;
    }
}



window.onload = function (){
    let canvasContainers = document.getElementsByClassName('animated-watch');
    window.watch = new Watch(canvasContainers[0]);
    canvasContainers[0].onmouseover = function(){
        watch.showBrand();
    }
    canvasContainers[0].onmouseout = function(){
        watch.showTime();
    }
    refreshDisplay();


    document.getElementById("stopbutton").onclick = function(){
        watch.stop();
    }
}





var angleControl = document.getElementById('angle-control');
angleControl.oninput = function() {
    watch.angle = +this.value;
    refreshDisplay();
};

var scaleControl = document.getElementById('scale-control');
scaleControl.oninput = function() {
    watch.scale = +this.value;
};

var topControl = document.getElementById('top-control');
topControl.oninput = function() {
    watch.top = +this.value;
    refreshDisplay();
};

var leftControl = document.getElementById('left-control');
leftControl.oninput = function() {
    watch.left = +this.value;
    refreshDisplay();
};

var skewXControl = document.getElementById('skewX-control');
skewXControl.oninput = function() {
    watch.skewX = +this.value;
    refreshDisplay();
};

var skewYControl = document.getElementById('skewY-control');
skewYControl.oninput = function() {
    watch.skewY = + this.value;
    refreshDisplay();
};

function refreshDisplay(){
    angleControl.value = document.getElementById('angle-control-val').innerText = watch.angle;
    scaleControl.value = document.getElementById('scale-control-val').innerText = watch.scale;
    topControl.value = document.getElementById('top-control-val').innerText = watch.top;
    leftControl.value = document.getElementById('left-control-val').innerText = watch.left;
    skewXControl.value = document.getElementById('skewX-control-val').innerText = watch.skewX;
    skewYControl.value = document.getElementById('skewY-control-val').innerText = watch.skewY;
}




